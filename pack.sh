#!/bin/bash
specname="rosa-secret-chamber.spec"
root="rosa-secret-chamber-1.1"
src="src"
src_tar_gz="./$root.tar.gz"

rm -rf ./$root


mkdir ./$root
mkdir ./$root/$src
mkdir ./$root/policy
mkdir ./$root/config
mkdir ./$root/l10n

cp -f pack.sh -f rosa-secret-chamber.spec ./$root
cp ./policy/* ./$root/policy
cp ./src/* ./$root/src
cp -r ./l10n/* ./$root/l10n
cp -r ./config/* ./$root/config

[ -f $src_tar_gz ] && rm -f $src_tar_gz
tar -cvzf $src_tar_gz "./$root"

rpmbuild --define="_sourcedir `pwd`"  $specname --nodeps -bs
rpmbuild --define="_sourcedir `pwd`"  $specname --ba
#rm -rf ./$root

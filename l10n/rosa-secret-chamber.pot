# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-06-09 10:31+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/getpasswd.py:20 src/rosa-secret-chamber.py:39
msgid "rosa-secret-chamber"
msgstr ""

#: src/getpasswd.py:23
msgid "Password required"
msgstr ""

#: src/getpasswd.py:33
msgid "ok"
msgstr ""

#: src/getpasswd.py:49
msgid "Wrong password"
msgstr ""

#: src/rosa-secret-chamber.py:75
#, python-format
msgid "Command: %s"
msgstr ""

#: src/rosa-secret-chamber.py:82
msgid "Run"
msgstr ""

#: src/rosa-secret-chamber.py:86
msgid "Cancel"
msgstr ""

#: src/rosa-secret-chamber.py:90 src/rosa-secret-chamber.py:140
#, python-format
msgid "Range: %s"
msgstr ""

#: src/rosa-secret-chamber.py:108
#, python-format
msgid "Bad range '%s'"
msgstr ""

#: src/rosa-secret-chamber.py:129
#, python-format
msgid "Can't run command, see log: %s"
msgstr ""

#: src/run-xephyr.py:34
#, python-format
msgid ""
"Could not use XAuthority file.\n"
"Most likely your system is not configured properly.\n"
"Refer to administrator manual to set it up.\n"
"Error: %s"
msgstr ""

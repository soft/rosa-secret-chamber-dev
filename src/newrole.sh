#!/bin/bash

export password=$3
export mlsrange=$1
export cmd=$2

LC_CTYPE=EN_US expect -c 'spawn /usr/bin/newrole -l $env(mlsrange) -- -c $env(cmd); expect "Password: "
send "$env(password)\r"
catch wait reason
set exit_status [lindex $reason 3]
interact
send_user "EXIT CODE: $exit_status\r"
exit $exit_status'

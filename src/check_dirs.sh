#!/bin/bash
runcon
error() { echo "ERROR:$1">> /dev/stderr; exit 1; }
assertret() {
if [ $? != 0 ];
then error $1 ;
fi; }

makedir(){
if [ ! -d "$1" ]; 
then
  mkdir -Z $2 "$1";
  assertret "Can not create $1";
else
  echo "Already exist: $1"
fi
}

if [ "$HOME" == "" -o ! -d "$HOME" ];
then
  error "bad HOME value: '$HOME'"
fi
echo $0 $1 $2

makedir $HOME/.secretchambers staff_u:object_r:rosa_secret_chamber_secretchambers_root_dir_t:s0
makedir $HOME/.secretchambers/$1 staff_u:object_r:rosa_secret_chamber_secretchamber_dir_t:$1
makedir $HOME/.secretchambers/$1tmp staff_u:object_r:rosa_secret_chamber_secretchamber_dir_t:$1

cp $HOME/.Xauthority $HOME/.secretchambers/$1
mkdir $HOME/.secretchambers/$1/.config


rm -rf $HOME/.secretchambers/$1/.config/openbox/*
rm -rf $HOME/.secretchambers/$1/.config/tint2/*

if [ "$2" == "True" ];
then
  export ob_config=openbox_nodecor
else
  export ob_config=openbox
fi;

mkdir -p $HOME/.secretchambers/$1/.config/openbox
cp  /usr/share/rosa-secret-chamber/config/$ob_config/* $HOME/.secretchambers/$1/.config/openbox/

if [ "$2" != "True" ];
then
  cp -R /usr/share/rosa-secret-chamber/config/tint2 $HOME/.secretchambers/$1/.config/
fi;


exit 0;

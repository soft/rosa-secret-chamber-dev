#!/usr/bin/python
from gi.repository import Gtk
from gi.repository import Gdk
import sys
import subprocess
import commands
import os
import selinux
import gettext 
import logging
from subprocess import Popen

gettext.bindtextdomain('rosa-secret-chamber', '/usr/share/rosa-secret-chamber/l10n')
gettext.textdomain('rosa-secret-chamber')
_ = gettext.gettext

class PasswordWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title=_("rosa-secret-chamber"))
        self.set_border_width(10)
        vbox = Gtk.HBox(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.label = Gtk.Label(_("Password required"))
        self.label.set_line_wrap(True)
        self.label.set_justify(Gtk.Justification.LEFT)
        vbox.pack_start(self.label, True, True, 0)
        self.entry = Gtk.Entry()
        self.entry.connect("activate", self.on_click_run)
        self.entry.set_visibility(False)
        self.entry.set_invisible_char("*")
        vbox.pack_start(self.entry, False, False, 0)
            
        btnRun = Gtk.Button(_("ok"))
        btnRun.connect("clicked", self.on_click_run)
        #vbox.pack_start(btnRun, True, True, 0)
        self.add(vbox)

        self.resize(500,110)
        self.set_resizable( False )
        
    def on_click_run(self, button):
        self.passwd=self.entry.get_text()
        self.hide()
        Gtk.main_quit()
def GetPasswd(retype=False):
    win = PasswordWindow()
    win.connect("delete-event", Gtk.main_quit)
    if retype:
        win.label.set_text(_("Wrong password"))
    win.show_all()
    Gtk.main()
    print(win.passwd)
    return win.passwd

if __name__ == "__main__":
    GetPasswd()

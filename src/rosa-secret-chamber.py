#!/usr/bin/python
from gi.repository import Gtk
from gi.repository import Gdk
import sys
import subprocess
import commands
import os
import selinux
import gettext
import logging
import getpasswd
from subprocess import Popen

home=os.getenv("HOME")
logname = '%s/.rosa-secret-chamber.log' % home
FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(filename=logname, level=logging.DEBUG, format=FORMAT)
#logging.debug("starting rosa-secret-chamber")
gettext.textdomain('rosa-secret-chamber')
_ = gettext.gettext



class ComboBoxWindow(Gtk.Window):

    def __init__(self):
        logging.debug('------------Starting--------------');
        self.cmdtoexec = ""
        if len(sys.argv) > 1:
            ret,self.cmdtoexec = commands.getstatusoutput("%s/deskopen.sh %s" % (rootpath, sys.argv[1]))
            if ret != 0:
                sys.exit(0)
        self.decoration = True
        if len(sys.argv) > 2:
            if sys.argv[2] == "1":
                self.decoration = False
        Gtk.Window.__init__(self, title=_("rosa-secret-chamber"))

        self.set_border_width(10)

        ret = -1
        retype=False
        self.passwd = "";
        while ret != 0:
            self.passwd=getpasswd.GetPasswd(retype)
            retype=True
            ret, out = commands.getstatusoutput('%s/newrole.sh  %s "%s" %s' % (rootpath, "s0", "/bin/echo", self.passwd))
        ret,out = commands.getstatusoutput("%s/get_available_ranges.sh %s 2>/dev/null" % (rootpath, self.passwd))
        if ret != 0:
            logging.error("error: code %d; %s" % (ret, out))
            sys.exit(0)

        strings = out.split("\n")
        self.D = {}
        self.mlsrange = "s0"

        for s in strings:
            ss = s.split()
            self.D[ss[1]] = ss[0]

        trans_range_store = Gtk.ListStore(int, str)

        for i,k in enumerate(self.D.keys()):
            trans_range_store.append([i, k])

        vbox = Gtk.HBox(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        hbox2 = Gtk.VBox(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)

        trans_range_combo = Gtk.ComboBox.new_with_model_and_entry(trans_range_store)
        trans_range_combo.connect("changed", self.on_trans_range_combo_changed)
        trans_range_combo.set_entry_text_column(1)

        self.label = Gtk.Label(_("Command: %s") % self.cmdtoexec)
        self.label.set_line_wrap(True)
        self.label.set_justify(Gtk.Justification.LEFT)
        vbox.pack_start(self.label, True, True, 0)

        vbox.pack_start(trans_range_combo, False, False, 0)

        btnRun = Gtk.Button(_("Run"))
        btnRun.connect("clicked", self.on_click_run)
        hbox2.pack_start(btnRun, True, True, 0)

        btncancel = Gtk.Button(_("Cancel"))
        btncancel.connect("clicked", self.on_click_cancel)
        hbox2.pack_start(btncancel, True, True, 0)

        self.range_label = Gtk.Label(_("Range: %s") % self.mlsrange)
        self.range_label.set_line_wrap(True)
        self.range_label.set_justify(Gtk.Justification.LEFT)

        vbox.pack_start(self.range_label, True, True, 0)
        vbox.pack_start(hbox2, True, True, 0)
        self.add(vbox)

        self.resize(500,110)
        self.set_resizable( False )

    def on_click_cancel(self, button):
        logging.debug('--------------Exit----------------');
        sys.exit(0)
    def on_click_run(self, button):
	print(self.decoration)
        errcode = self.runcon ("staff_u:staff_r:rosa_secret_chamber_check_dirs_t:%s" % self.mlsrange, "%s/check_dirs.sh" % rootpath, self.mlsrange, str(self.decoration))
        if errcode != 0:
            self.range_label.set_text(_("Bad range '%s'") % (self.mlsrange))
            return
        selinux.setexeccon("\n");
        self.hide()
        while Gtk.events_pending():
            Gtk.main_iteration()
        os.environ["HOME"] = "%s/.secretchambers/%s" % (home, self.mlsrange)
        os.environ["TMP"] = "%s/.secretchambers/%stmp" % (home, self.mlsrange)
#        errcode = self.runcon ("staff_u:staff_r:staff_t:s0", "/usr/share/rosa-secret-chamber/run.py"  , self.mlsrange, self.cmdtoexec)
        #if self.cmdtoexec == "":
        #self.cmdtoexec = "ck-launch-session openbox-session"
        #else:
        #    self.cmdtoexec =  "'ck-launch-session dbus-launch openbox-session & " +self.cmdtoexec

        ret,out = commands.getstatusoutput("echo 'dbus-launch %s &' >> %s" %(self.cmdtoexec, "%s/.secretchambers/%s/.config/openbox/autostart" % (home, self.mlsrange)))
        #errcode = self.runcon ("staff_u:staff_r:staff_t:s0", "/usr/share/rosa-secret-chamber/seunshare", "-h", "%s/.secretchambers/%s" % (home, self.mlsrange),"-t", "%s/.secretchambers/%stmp" % (home, self.mlsrange), "--","/usr/share/rosa-secret-chamber/newrole.sh ", self.mlsrange  , "/usr/share/rosa-secret-chamber/run.py %s" % "'exec ck-launch-session openbox-session'" , self.passwd)
        self.ccc = "{0}/seunshare -h {1}/.secretchambers/{2} -t {1}/.secretchambers/{2}tmp -- {0}/newrole.sh {2} '{0}/run.py \"openbox-session\"' {3} ".format(rootpath, home, self.mlsrange, self.passwd)
        print(self.ccc)
        ret,out = commands.getstatusoutput(self.ccc)
#        output = process.communicate()[0]
        if ret != 0:
            self.range_label.set_text(_("Can't run command, see log: %s") % logname)
            self.show()
            return
        sys.exit(0)
    def on_trans_range_combo_changed(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter != None:
            self.mlsrange = self.D[combo.get_active_text()]
        else:
            entry = combo.get_child()
            self.mlsrange = entry.get_text()
        self.range_label.set_text(_("Range: %s") % self.mlsrange)
    def runcon(self, context, program, *args):
        try:
            errcode = selinux.setexeccon(context)
        except OSError as e:
            logging.error('Can not set context %s' % context)
            errcode = e.errno
        if errcode:
            return errcode
        progargs = (program,) + args
        try:
            errcode = os.spawnv(os.P_WAIT, program, progargs)
            if errcode != 0:
                logging.error('child not run ' + str(progargs) + ': code %d' % errcode)
        except OSError as e:
            logging.error('child not run ' + str(progargs) + ': ' + e.message)
            errcode = e.errno
        return errcode
try:
    win = ComboBoxWindow()
    win.connect("delete-event", Gtk.main_quit)
    win.show_all()
    Gtk.main()
except OSError as e:
    logging.error('error: %s' + e.message)

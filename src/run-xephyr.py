#! /usr/bin/python -Es
__author__ = 'rosa'
import os
import sys
import subprocess
import getpass
import commands
import shlex
import gettext

DEBUG = False

gettext.textdomain('rosa-secret-chamber')
_ = gettext.gettext

def execute(command):
    xauthpath = os.getenv("XAUTHORITY")
    try:
        if xauthpath:
            directory = os.path.dirname(xauthpath)
            if not os.path.exists(directory):
                ### bin_t should transit to user_t,
                ### then subprocess call creates the directory with the user_tmp_t label
                ### NEED-A-FIX with namespace.init issue
                subprocess.Popen("/bin/mkdir %s " % directory, shell=True)
                #os.makedirs(directory)
            subprocess.Popen("/bin/touch %s " % xauthpath, shell=True)
            #open(xauthpath, 'a').close()
        else:
            open(os.path.expanduser('~/.Xauthority'), 'a').close()
    except Exception as ex:
        print(_('Could not use XAuthority file.\n'
                'Most likely your system is not configured properly.\n'
                'Refer to administrator manual to set it up.\nError: %s') % (str(ex)))
        sys.exit(1)
    xauth = os.getenv("XAUTHTMP")
    if DEBUG: print xauth
    if xauth:
        subprocess.Popen("xauth add %s" % xauth, shell=True)
    screen = os.popen("xdpyinfo | grep -B1 resolution").readlines()
    dbus = subprocess.Popen("/usr/bin/dbus-launch --exit-with-session", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for var in dbus.stdout:
    	sp = var.split('=', 1)
        if DEBUG: print sp
    	os.environ[sp[0]] = sp[1][:-1]

    wxh = screen[0].split()[1]
    width,height = wxh.split("x")
    dpi = screen[1].split()[1]
    if DEBUG: print(dpi)
    if DEBUG: print ("Current resolution is %s x %s" % (width,height))
    #make 2/3 resolution
    w=640
    h=480
    try:
        w=int(width)//3*2
        h=int(height)//3*2
    except ValueError:
        if DEBUG: print("can't get resolution")
        if DEBUG: print(w, h)
    read_displayfd, write_displayfd = os.pipe()
    cmd = "/usr/bin/Xephyr -nolisten tcp -title {title} -resizeable -screen {screen} -dpi {dpi} -displayfd {dfd}".format(dfd=write_displayfd, title="rosa-secret-chamber",screen="%sx%s" % (w,h), dpi=dpi) 
    if DEBUG: print cmd
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    readfile = os.fdopen(read_displayfd)
    port = readfile.readline()
    if DEBUG: print port
    if port:
	os.environ["DISPLAY"] = ":" + port
	os.environ["XDG_CONFIG_HOME"] = "/usr/share/rosa-secret-chamber/config"
	process2 = subprocess.Popen("DISPLAY=:%s openbox-session" % port,shell=True)
	del os.environ["XDG_CONFIG_HOME"]
	process3 = subprocess.Popen("DISPLAY=:%s %s" % (port,command),shell=True)

    out, err = process.communicate()
    exitCode = process.returncode
    if DEBUG: print out, err

    return exitCode

def run(cmd):
    c='"%s"' % (cmd)
    execute(c)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        if DEBUG:
            print("args>>>>>>>>>>>>>>")
            print(sys.argv)
        run(sys.argv[1])

#!/bin/bash

if [ -z $1 ]
then
    echo "not enough params"
    exit 1
fi

if [[ $1 == *.desktop ]]
then
    CMD=`grep '^Exec' -m 1 $1 | awk -F[=\ ] '{ print $2 }'`
else
    CMD=$1
fi
echo $CMD

export XAUTHTMP=`xauth list $DISPLAY`;
CONTEXT=`id -Z`
CURDOMAIN=`echo $CONTEXT | awk -F[:] '{ print $3 }'` 
CURROLE=`echo $CONTEXT | awk -F[:] '{ print $2 }'`
DEFAULTLEVEL="s1"
newrole-gui -p -g -r $CURROLE -t $CURDOMAIN -l $DEFAULTLEVEL -sc $CONTEXT -c "/usr/share/rosa-secret-chamber/run-xephyr.py $CMD"


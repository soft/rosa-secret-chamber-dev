#!/bin/bash
export password=`python getpasswd.py`
export mlsrange=$1
export cmd=$2
expect -c 'spawn newrole -l $env(mlsrange) -- -c $env(cmd); expect "Password: "
send "$env(password)\r"
interact'


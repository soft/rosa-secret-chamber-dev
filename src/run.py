#! /usr/bin/python -Es
__author__ = 'rosa'
import os
import sys
import subprocess
import getpasswd
import commands
def execute(command):
    screen = os.popen("xdpyinfo | grep -B1 resolution").readlines()
    wxh = screen[0].split()[1]
    width,height = wxh.split("x")
    dpi = screen[1].split()[1]
    print(dpi)
    print ("Current resolution is %s x %s" % (width,height))
    #make 2/3 resolution
    w=640
    h=480
    try:
        w=int(width)//3*2
        h=int(height)//3*2
    except ValueError:
        print("can't get resolution") 
        print(w, h)
    process = subprocess.Popen("/usr/bin/Xephyr -nolisten tcp -keybd ephyr,,,xkbmodel=evdev,xkblayout=us,xkbrules=evdev "
                               "-title '%s' -resizeable -screen %s -dpi %s -displayfd 5 5>&1 2>/dev/null"
                               % ("rosa-secret-chamber", "%sx%s" % (w,h), dpi),
                               shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    # Poll process for new output until finished
    while True:
        port = process.stdout.readline()
        if port == '' and process.poll() != None:
            break
        process2 = subprocess.Popen("export DISPLAY=:%s %s" %(port, command),
                               shell=True)
#        sys.stdout.write(nextline)
#        sys.stdout.flush()

    output = process.communicate()[0]
    exitCode = process.returncode

    if (exitCode == 0):
        return output
 #   else:
 #       raise ProcessException(command, exitCode, output)
def run(cmd):
    c='"%s"' % (cmd)
    execute(c)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        print("args>>>>>>>>>>>>>>")
        print(sys.argv)
        run(sys.argv[1])

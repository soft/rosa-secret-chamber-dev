Summary: Rosa secret chamber
Name: rosa-secret-chamber
Version: 1.1
Release: 26
License: GPL
Group: System/Base
Url:		https://abf.rosalinux.ru/soft/%{name}-dev
Source0:	https://abf.io/soft/%{name}-dev/archive/%{name}-dev-%{version}.tar.gz
Requires: policycoreutils-python
Requires: expect
Requires: python-selinux
Requires: newrole-gui
Requires: gettext
Requires(post): selinux-policy-devel

%description
Rosa secret chamber package is the solution of working in GUI with selinux.

%prep
%setup -qn %{name}-dev-%{version}
msgfmt -o l10n/ru/LC_MESSAGES/rosa-secret-chamber.mo l10n/ru/LC_MESSAGES/rosa-secret-chamber.po


%install
%define sharedir %{buildroot}/%{_datadir}/%{name}
install -d %{sharedir}
install -d %{sharedir}/policy
install -m644 policy/* %{sharedir}/policy
install -m755 src/run-xephyr.py %{sharedir}
install -m755 src/rosa-secret-chamber.sh %{sharedir}
install -d %{buildroot}/%{_sbindir}
ln -f -s %{_datadir}/%{name}/rosa-secret-chamber.sh %{buildroot}%{_sbindir}/rosa-secret-chamber

install -d config %{sharedir}/config
install -d config/openbox %{sharedir}/config/openbox
install -d config/openbox_nodecor %{sharedir}/config/openbox_nodecor
install -m644 config/openbox/* %{sharedir}/config/openbox
install -m644 config/openbox_nodecor/* %{sharedir}/config/openbox_nodecor
install -d config/tint2 %{sharedir}/config/tint2
install -m644 config/tint2/* %{sharedir}/config/tint2

install -d %{buildroot}/%{_datadir}/locale/ru/LC_MESSAGES
install -m 644 l10n/ru/LC_MESSAGES/*.mo %{buildroot}/%{_datadir}/locale/ru/LC_MESSAGES/

%find_lang %{name}

#%preun 
#if [ $1 == 0 ]; then
#make NAME=mls -C %{_datadir}/%{name}/policy/ -f %{_datadir}/selinux/devel/Makefile clean
#semodule -s mls -r rosa-secret-chamber
#fi
#true

%post
make NAME=mls -C %{_datadir}/%{name}/policy/ -f %{_datadir}/selinux/devel/Makefile
semodule -s mls -i %{_datadir}/%{name}/policy/rosa-secret-chamber.pp
restorecon -F -R %{_datadir}/%{name}/ 


%files -f %{name}.lang
%defattr(-,root,root)
%dir %{_datadir}/%{name}/policy
%{_datadir}/%{name}/policy/*
%{_datadir}/%{name}/*.py*
%{_datadir}/%{name}/*.sh*
%{_sbindir}/rosa-secret-chamber
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/config
%dir %{_datadir}/%{name}/config/openbox
%dir %{_datadir}/%{name}/config/openbox_nodecor
%{_datadir}/%{name}/config/openbox_nodecor/*
%{_datadir}/%{name}/config/openbox/*
%dir %{_datadir}/%{name}/config/tint2
%{_datadir}/%{name}/config/tint2/*

%changelog

